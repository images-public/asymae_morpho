# Part-based approximations for morphological operators using asymmetric auto-encoders
## Bastien Ponchon, Santiago Velasco-Forero, Samy Blusseau, Jesus Angulo and Isabelle Bloch
Center for Mathematical Morphology, Mines ParisTech, PSL REsearch University and Télécom ParisTech, Université Paris-Saclay, France

